from django.db import models
from timezone_field import TimeZoneField


class Mailing(models.Model):
    text = models.TextField()
    filter_tag = models.CharField(max_length=30)
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()


class Client(models.Model):
    phone = models.IntegerField()
    operator_code = models.IntegerField()
    tag = models.CharField(max_length=100)
    timezone = TimeZoneField(default="Europe/Moscow")


class Message(models.Model):
    STATUSES_CHOICES = (
        ("OK", "OK"),
        ("ERROR", "ERROR")
    )

    send_time = models.DateTimeField(auto_now_add=True)
    status = models.CharField(choices=STATUSES_CHOICES, max_length=10)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
